# Read an integer:
# a = int(input())
# Print a value:
# print(a)
num= int(input())
a = [["."]*num for i in range(num)]
for y in range(num):
    for x in range(num):
        if x == y:
            a[y][x] = '*'
        elif x == (num-1)/2:
            a[y][x] = '*'
        elif x + y == 6:
            a[y][x] = '*'
        elif y == (num-1)/2:
            a[y][x] = '*'
for li in a:
    print(*li)